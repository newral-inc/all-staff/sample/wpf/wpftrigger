﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTrigger
{
    /**
     * @brief パーソンクラス
     *        データグリッドに表示するクラス
     */
    public class Person
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        /**
         * @brief コンストラクタ
         */
        public MainWindow()
        {
            InitializeComponent();

            // データグリッドにサンプルデータを設定します。
            var list = new List<Person>();
            list.Add(new Person() { Name = "佐藤", Address = "東京", Age = 20 });
            list.Add(new Person() { Name = "鈴木", Address = "千葉", Age = 31 });
            list.Add(new Person() { Name = "高橋", Address = "埼玉", Age = 42 });
            list.Add(new Person() { Name = "田中", Address = "神奈川", Age = 53 });
            list.Add(new Person() { Name = "伊藤", Address = "群馬", Age = 64 });
            dataGrid.ItemsSource = list;
        }

        /**
         * @brief データグリッドでマウスが移動した時に呼び出されます。
         * 
         * @param [in] sender データグリッド
         * @param [in] e マウスイベント
         */
        private void DataGrid_MouseMove(object sender, MouseEventArgs e)
        {
            // マウスカーソル位置にあるデータグリッドのセルを取得します。
            var dataGrid = sender as DataGrid;
            var point = e.GetPosition(dataGrid);
            var cell = GetDataGridCell<DataGridCell>(dataGrid, point);
            if (cell == null)
            {
                return;
            }

            // トリガーオブジェクトを生成します。
            // マウスオーバーで背景色を変更します。
            var trigger = new Trigger();
            trigger.Property = DataGridCell.IsMouseOverProperty;
            trigger.Value = true;
            var brush = new SolidColorBrush(Colors.Green);
            var setter = new Setter(TextBox.BackgroundProperty, brush);
            trigger.Setters.Add(setter);

            // セルとトリガーはスタイルで関連付けます。
            var style = new Style(typeof(DataGridCell));
            style.Triggers.Add(trigger);
            cell.Style = style;
        }

        // 内部メソッド(詳細は省略)

        private T GetDataGridCell<T>(DataGrid dataGrid, Point point)
        {
            T result = default(T);
            var hitResultTest = VisualTreeHelper.HitTest(dataGrid, point);
            if (hitResultTest != null)
            {
                var visualHit = hitResultTest.VisualHit;
                while (visualHit != null)
                {
                    if (visualHit is T)
                    {
                        result = (T)(object)visualHit;
                        break;
                    }
                    visualHit = VisualTreeHelper.GetParent(visualHit);
                }
            }
            return result;
        }
    }
}
